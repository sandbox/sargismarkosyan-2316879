# jQuery MobileMenu
This module integrates the MobileMenu jQuery plugin.
(http://sargismarkosyan.github.io/MobileMenu/)
It's create library, so you can use it programmatically, also its have
admin panel where you can choose in which themes library mast be load.


## Requirements
* jQuery update. Plugin requires jQuery version 1.7+
  https://www.drupal.org/project/jquery_update
* Libraries https://www.drupal.org/project/libraries


## Installation
* Install as you would normally install a contributed drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7
  for further information.
* Download the MobileMenu library from
  https://github.com/sargismarkosyan/MobileMenu/
* Place the library in the appropriate directory E.G.
  sites/all/libraries/jquery.mobilemenu
  sites/all/libraries/jquery.mobilemenu/jquery.mobilemenu.css
  sites/all/libraries/jquery.mobilemenu/jquery.mobilemenu.js


## Usage

For import library progromaticaly use this code
drupal_add_library('jquery_mobilemenu', 'jquery.mobilemenu');

Or if you want to use in full theme use administration config page
(you can reach it by going Administration � Configuration � Development
� jQuery MobileMenu or more shortly go by this address
[Path to site]/admin/config/development/jquery_mobilemenu
where [Path to site] is your site address).

There you can choose themes where library will be automatically added,
by default it's site's default theme.


## Plugin Usage
After including the library, write this code somewhere on your theme
javascript file.

```javascript
$('.menu').mobilemenu();
```

More about plugin you can read in plugin's page
http://sargismarkosyan.github.io/MobileMenu/
